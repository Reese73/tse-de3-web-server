# Project Cloud computing

# Sujet : Réplication de serveur web avec Amazon Web Service
*Explain the concepts and demonstrate what AWS proposes to its customer for elasticity and high availability :*

*The services and keywords of AWS that you will have to work on are :*
- *LaunchConfiguration and AutoScalingGroup*
- *Elastic Load Balancing (ELB)*

*The ideal final deliverable would be a CloudFormation template enabling a stack with for example a Load Balancer in front of a pool of EC2 instances hosting an HTTP Server*

## Code source  :

Notre Rende ce compose de : 
- un script YAML de création d'un pile AWS CloudFormation
- un fichier index.html contenant la page principale du serveur web
- un script bash permettant de gérer la pile et les bucket S3 AWS
- un script bash conteant les commandes qui s'éxécute au lancement des instances EC2

Le code source est disponible à cette adresse : https://gitlab.com/Reese73/tse-de3-web-server.git

---
**Pré-requis** : 

- AWS CLI version 2
- Interpréteur de commande bash
- identifiants AWS enregistrés dans .aws/credentials

---
# User guide :  

Pour lancer la création de l'infrastructure AWS vous devez :

1. Modifier les variables du fichier "init_web_server.sh"
2. Modifier les paramêtres du fichier "init_web_server.yaml"
3. Lancer le script bash "init_web_server.sh" avec le paramêtre : "init"
4. Lancer le script bash "init_web_server.sh" avec le paramêtre : "dns"
    Cette commande peut renvoyer "None" le temps que la pile se créer : Attendez quelques instants et réessayer
5. Accèder au web serveur via l'adresse retournée via votre navigateur

Le temps que les instances EC2 s'initialisent, l'adresse peut renvoyer une erreur "Bad Gataway". 
Attendez quelques instants et réessayer.

Vous pouvez ensuite géré votre pile ainsi que le bucket créer avec le même script bash en utilisant les autres paramêtres autorisés : 
- init : *créer le bucket et upload le fichier "index.html" & creer la pile*
- delete-web-server : *supprime la pile*
- delete-bucket : *supprime le bucket et son contenu*
- replace-html "<your_new_path>" : *remplace le fichier "index.html" sur le bucket*
- dns : *récupérer l'adresse du dns permettant d'accéder au server web*



