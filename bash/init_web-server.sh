#!/bin/bash

#Variables à modifier
html_file=/Users/alexandrelassiaz/Documents/Ressources_TD_TSE/Cloud_computing_AWS/ProjetCloud/html/index.html
template_file=/Users/alexandrelassiaz/Documents/Ressources_TD_TSE/Cloud_computing_AWS/ProjetCloud/yaml/init-web-server.yaml

keyName=LASSAZ_1
bucket_name=tse-de3-web-server-data
stack_name=LoadBalanced-WebServer

action=$1

case $action in
    init)
        aws s3 mb s3://$bucket_name
        aws s3 cp $html_file s3://$bucket_name
        aws cloudformation create-stack --stack-name $stack_name --template-body file://$template_file --parameters ParameterKey=KeyName,ParameterValue=$keyName
    ;;

    delete-web-server)
        aws s3 rb s3://$bucket_name --force
        aws cloudformation delete-stack --stack-name $stack_name
    ;;
    replace-html)
        html_file = $2
        aws s3 cp $html_file s3://$bucket_name
    ;;
    dns)
        aws cloudformation describe-stacks --stack-name $stack_name --query "Stacks[0].Outputs[5].OutputValue" --output text
    ;;
  *)
    echo  "Please provide correct arguments : 'init' or 'delete-web-server' or 'replace-html <your_new_path>' or 'dns'"
    ;;
esac